package plzKrakenGibeFeature;

import java.util.Random;

enum City {
	Tokyo,
	Venice,
	ElCairo,
	Sydney
}

public class Kraken implements SeaMonster {
	
	public Kraken(float attackAccuracy, int numberOfAttack) throws NotAProbabilityException {
		if (attackAccuracy < 0 || attackAccuracy > 1)
			throw new NotAProbabilityException(attackAccuracy);
		this.attackAccuracy = attackAccuracy;
		this.numberOfAttack = numberOfAttack;
	}
	
	private final float attackAccuracy;
	private final int numberOfAttack;
	
	public void destroyCity(City city, String cityNickname) {
		System.out.println(cityNickname + " has been wasted!");
		System.out.println("destruction score = " + getDesctructionScore());		
	}
	
	
	public double getDesctructionScore() {
		Random random = new Random();
		double destructionScorePerAttack = random.nextDouble();
		
		double destructionScore = 0;
		for (int i = 0; i < numberOfAttack; ++i) {
			if (bernoulliCoinToss(attackAccuracy))
				destructionScore += destructionScorePerAttack;
		}
		
		return destructionScore;
	}
	
	public boolean bernoulliCoinToss(float p) {
		Random random = new Random();
		float f = random.nextFloat();
		return (f - (int) f) <= attackAccuracy;
	}
}


class NotAProbabilityException extends Exception {
	
	public NotAProbabilityException(float p) {
		super();
		this.p = p;
	}

	private float p;
	
	@Override
	public String toString() {
		return "" + p + " is not a valid probability";
	}
	
}